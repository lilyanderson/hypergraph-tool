# Hypergraph Tool
This project is designed to aid in the visualization of hypergraphs. 
The primary goal is to be able to completely visualize the aspects of Dr. Stephen Wolfram's [project to find the fundamental theory of physics](https://www.wolframphysics.org/).
The introduction of which can be found [here](https://writings.stephenwolfram.com/2020/04/finally-we-may-have-a-path-to-the-fundamental-theory-of-physics-and-its-beautiful/).

## Current work
This program is capable of rendering a hypergraph on the screen, add and delete arbitrary edges, find a path between any two nodes, calculate the distance between any two nodes, draw the set of every node that is at most graph distance r from a specified node, and update the graph per Wolfram.

### Command list

- add edge nodes
- delete edge nodes
- path a b
- ball node radius
- distance a b
- update [count]
- vr node max-dist
- averaged vr max-dist
- clear

## Future work
Focusing on trying to visualize Wolfram's work, some of the near term goals are:

- Statistical analysis on graph dimensionality and curvature (as defined by Wolfram)
- Identify local neighborhoods in the graph

Other graph related properties are welcome as suggestions.

## Building
To build this project, you will need the gtk development tools **GTK+**, **Glib** and **Cario** to be installed.
### Debian and Ubuntu
``` shell
>sudo apt install libgtk-3-dev
```
