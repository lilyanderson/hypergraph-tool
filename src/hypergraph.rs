use std::fmt;
use std::collections::{HashMap,HashSet};

use crate::rule::{self, Rule};

//Represents the mathematical concepter of a hypergraph. It is related to
//ordinary graphs, except edges are allowed to have many elements and not just
//two.
#[derive(Clone)]
pub struct Hypergraph {
    pub graph: Vec::<Vec::<usize>>,
    pub total_nodes: usize,
}

impl Hypergraph {
    pub fn new() -> Hypergraph {
        let graph = Vec::new();
        let total_nodes = 0;
        Hypergraph { graph, total_nodes }
    }

    pub fn matches(&self, other: &Hypergraph) -> bool {
        if self.total_nodes != other.total_nodes || self.graph.len() != other.graph.len() {
            return false;
        }

        for (edge_a, edge_b) in self.graph.iter().zip(other.graph.iter()) {
            for (node_a, node_b) in edge_a.iter().zip(edge_b.iter()) {
                if node_a != node_b {
                    return false;
                }
            }
        }
        true
    }

    //Takes two nodes and an adjacency matrix. If a path between a and b exists
    //then this will return the path, otherwise returns None. Note this assumes
    //all edges are distance 1.
    pub fn dijkstra(a: usize, b: usize, adjacency: Vec::<Vec::<usize>>) -> Option::<Vec::<usize>> {
        if a >= adjacency.len() || b >= adjacency.len() {
            return None;
        }
        let mut dist = Vec::new();
        let mut vertexes = Vec::new();
        let mut prev: Vec::<Option::<usize>> = Vec::new();

        let total_nodes = adjacency.len();
        for node in 0..total_nodes {
            dist.push(total_nodes);
            prev.push(None);
            vertexes.push(node);
        }
        dist[a] = 0;

        while vertexes.len() != 0 {
            let mut u = vertexes[0];
            let mut index = 0;
            for (i, node) in vertexes.iter().enumerate() {
                if dist[*node] < dist[u] {
                    u = *node;
                    index = i;
                }
            }
            //Short circuit the algorithm, the path to b is currently the shortest.
            if u == b {
                break;
            }
            vertexes.remove(index);
            for (node, adj) in adjacency[u].iter().enumerate() {
                if *adj == 1 {
                    let alt = dist[u] + 1;
                    if alt < dist[node] {
                        prev[node] = Some(u);
                        dist[node] = alt;
                    }
                }
            }
        }
        if prev[b] == None {
            None
        } else {
            let mut previous = b;
            let mut path = Vec::new();
            path.insert(0,b);
            while previous != a {
                previous = prev[previous].unwrap();
                path.insert(0,previous);
            }
            Some(path)
        }
    }

    pub fn find_vr(node: usize, max: usize, adjacency: &Vec::<Vec::<usize>>) -> Vec::<usize> {
        let mut visited = HashSet::new();
        let mut current: HashSet::<usize> = HashSet::new();
        let mut next = HashSet::new();
        let mut vr = Vec::new();
        next.insert(node);
        for _ in 0..max {
            for node in &current {
                for node in find_all_neighbors(*node, adjacency) {
                    next.insert(node);
                }
            }
            current = next;
            next = HashSet::new();
            for node in &current {
                visited.insert(*node);
            }
            vr.push(visited.len());
        }
        vr
    }

    pub fn find_all_vr(max: usize, adjacency: &Vec::<Vec::<usize>>) -> Vec::<Vec::<usize>> {
        let mut vrs = Vec::new();
        for node in 0..adjacency.len() {
            vrs.push(Hypergraph::find_vr(node, max, adjacency));
        }
        vrs
    }

    pub fn average_vr(max: usize, adjacency: &Vec::<Vec::<usize>>) -> Vec::<f64> {
        let vrs = Hypergraph::find_all_vr(max, adjacency);
        let mut counts = Vec::new();
        let mut result = Vec::new();

        for _ in 0..max {
            counts.push(0);
        }
        for vr in &vrs {
            for i in 0..max {
                counts[i] += vr[i];
            }
        }

        let total = if vrs.len() != 0 {
            vrs.len() as f64
        } else {
            1.0 //only an issue if graph is empty
        };
        for count in counts {
            result.push(count as f64 / total);
        }
        result
    }

    //Given a node and a radius, find all paths of graph distance radius
    pub fn ball(node: usize, radius: usize, adjacency: &Vec::<Vec::<usize>>) -> Vec::<Vec::<usize>> {
        let mut paths = Vec::new();
        let mut next = Vec::new();
        let mut path = Vec::new();
        let mut visited = HashSet::new();
        path.push(node);
        paths.push(path);

        for _ in 0..radius {
            for path in &paths {
                let last = path[path.len() - 1];
                if visited.contains(&last) {
                    continue;
                }
                visited.insert(last);
                for node in find_all_neighbors(last, &adjacency) {
                    let mut res = path.clone();
                    res.push(node);
                    next.push(res);
                }
            }
            for path in next {
                paths.push(path);
            }
            next = Vec::new();
        }
        paths
    }

    //Updates self according to rule. Looks for features of the graph are
    //homomorphic to rule.lhs and replaces them with rule.rhs. All other edges
    //of the graph are preserved.
    pub fn update(&mut self, rule: &Rule) {
        let matching = Self::find_matching(&self, rule);
        let mut used = HashSet::new();
        let mut updated = Vec::new();
        //Update every non-overlaping match. Do so in a greedy fashion.
        for (subgraph, map) in matching {
            if overlaps(&subgraph, &used) {
                continue;
            }
            for edge in subgraph {
                used.insert(edge);
            }
            let (result, total) = rule.create_rhs(&map, self.total_nodes);
            for edge in result {
                updated.push(edge);
            }
            self.total_nodes = total;
        }
        //Include every edge which was not in a match.
        for edge in 0..self.graph.len() {
            if !used.contains(&edge) { 
                updated.push(self.graph[edge].clone());
            }
        }

        updated.sort_by(|a,b| {
            let a = vec_to_val(&a, self.total_nodes);
            let b = vec_to_val(&b, self.total_nodes);
            a.cmp(&b)
        });

        self.graph = updated;
    }

    /* Finds all possible updates to a graph where the rule is applied a single time. */
    pub fn all_updates(&self, rule: &Rule) -> Vec::<Hypergraph> {
        let matching = Self::find_matching(&self, rule);
        let mut results = Vec::new();
        for (subgraph, map) in matching {
            let mut used = HashSet::new();
            let mut updated = Vec::new();
            for edge in subgraph {
                used.insert(edge);
            }
            let (result, total) = rule.create_rhs(&map, self.total_nodes);
            for edge in result {
                updated.push(edge);
            }
            for other_edge in 0..self.graph.len() {
                if !used.contains(&other_edge) {
                    updated.push(self.graph[other_edge].clone());
                }
            }
            let graph = Hypergraph { graph: updated, total_nodes: total };
            results.push(graph.get_cannonical_form());
        }
        results
    }

    //This method finds every subgraph of self that is homomorphic to rule.lhs
    //and returns the subgraph along with the homomorphism.
    fn find_matching(graph: &Hypergraph, rule: &Rule) -> Vec::<(Vec::<usize>, HashMap::<usize, usize>)> {
        let subgraphs = subgraphs(graph.graph.len(), rule.lhs.len());
        let mut result = Vec::new();
        for subgraph in &subgraphs {
            let mut sub: Vec::<Vec::<usize>> = Vec::new();
            for edge in subgraph {
                sub.push(graph.graph[*edge].clone());
            }
            if let Some(map) = match_graphs(&rule.lhs, &sub) {
                result.push((subgraph.clone(), map));
            }
        }
        result
    }

    pub fn insert_edge(&mut self, edge: Vec::<usize>) {
        for node in &edge {
            if *node >= self.total_nodes {
                self.total_nodes = *node + 1;
            }
        }
        self.graph.push(edge);
    }

    //Finds and deletes the first occurance of an edge within the graph.
    pub fn delete_edge(&mut self, edge: Vec::<usize>) {
        let mut to_del = self.graph.len();
        for (i, comp) in self.graph.iter().enumerate() {
            if edge == *comp {
                to_del = i;
                break;
            }
        }
        if to_del < self.graph.len() {
            let to_del = self.graph.remove(to_del);
            for node in to_del {
                if node == self.total_nodes - 1 {
                    self.remove_unused();
                    break;
                }
            }
        }
    }

    //If the largest node is not used in any edge, there is no reason to 
    //include it in total_nodes.
    fn remove_unused(&mut self) {
        let mut max = 0;
        for edge in &self.graph {
            for node in edge {
                if *node > max {
                    max = *node;
                }
            }
        }
        self.total_nodes = max + 1;
    }

    fn get_cannonical_form(&self) -> Hypergraph {
        let mut parts = rule::get_rule_parts(&self.graph);
        let mut mapping = HashMap::new();
        let sorted = rule::get_sorted(&mut parts, &mut mapping, &mut 0);
        Hypergraph {
            graph: sorted,
            total_nodes: self.total_nodes
        }
    }
}

fn vec_to_val(vec: &Vec::<usize>, base: usize) -> usize {
    let mut total = 0;
    let mut pow = 1;
    for num in vec {
        total += pow * num;
        pow *= base;
    }
    total
}

//Returns true if a subgraph overlaps with a portion of the graph that has
//already been used by something else.
fn overlaps(subgraph: &Vec::<usize>, used: &HashSet::<usize>) -> bool {
    for edge in subgraph {
        if used.contains(edge) {
            return true;
        }
    }
    false
}

//Takes a graph and returns every ordered subgraph of size n
fn subgraphs(total: usize, n: usize) -> Vec::<Vec::<usize>> {
    let mut result = Vec::new();
    if n == 1 {
        for edge in 0..total {
            result.push(vec![edge]);
        }
    } else {
        let sub_minus_1 = subgraphs(total, n-1);
        for edge in 0..total {
            for subgraph in &sub_minus_1 {
                if subgraph.contains(&edge) {
                    continue;
                }
                let mut appended = subgraph.clone();
                appended.push(edge);
                result.push(appended);
            }
        }
    }
    result
}

// Takes two graphs and attempts to create a homomorphism from graph_1 to
// graph_2. This function does not attempt to permute the edges of either graph
// to do so.
fn match_graphs(graph_1: &Vec::<Vec::<usize>>, graph_2: &Vec::<Vec::<usize>>) -> Option::<HashMap::<usize, usize>> {
    if graph_1.len() != graph_2.len() {
        //Graphs cannot match as they have a different number of edges.
        return None;
    }
    let mut sur = HashMap::new();
    for (a,b) in graph_1.iter().zip(graph_2.iter()) {
        match match_edges(a, b, &sur) {
            Some(map) => { 
                for (key, val) in map.iter() {
                    sur.insert(*key, *val);
                }
            }
            None => { return None; } //Graphs cannot match as an edge didn't
        }
    }
    Some(sur)
}

//Takes two edges and attempts to create a surjection from edge_1 to edge_2.
//This is used in finding a homomorphism from one graph to another.
fn match_edges(edge_1: &Vec::<usize>, edge_2: &Vec::<usize>, sur: &HashMap::<usize,usize>) -> Option::<HashMap::<usize, usize>> {
    let mut result = sur.clone();
    if edge_1.len() != edge_2.len() {
        //Edges cannot match as they are different arities.
        None
    } else {
        for (a,b) in edge_1.iter().zip(edge_2.iter()) {
            if let Some(val) = result.get(a) {
                if *val != *b {
                    //Node was mapped to a contradictory value.
                    return None;
                }
            } else {
                result.insert(*a,*b);
            }
        }
        Some(result)
    }
}

//Take hypergraph and construct a string representing it.
impl fmt::Display for Hypergraph {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut result = String::from("{");
        for edge in &self.graph {
            result.push('{');
            for node in edge {
                result.push_str(&format!("{},", node));
            }
            result.pop();
            result.push_str("},");
        }
        result.pop();
        result.push('}');
        write!(f, "Nodes: {}\t{}", self.total_nodes, result)
    }
}

//Creates an adjacency matrix where a is adjacent to b if a and b occur in
//the same edge anywhere in the graph.
pub fn adjacency_matrix(graph: &Vec::<Vec::<usize>>, total_nodes: usize) -> Vec::<Vec::<usize>> {
    let mut matrix: Vec::<Vec::<usize>> = Vec::with_capacity(total_nodes);
    for _ in 0..total_nodes {
        let mut row = Vec::with_capacity(total_nodes);
        for _ in 0..total_nodes {
            row.push(0);
        }
        matrix.push(row);
    }
    for edge in graph {
        for a in edge {
            for b in edge {
                matrix[*a][*b] = 1;
            }
        }
    }
    matrix
}

//Creates a directed adjacency matrix where a is adjacent to b if a 
//preceeds b in an edge. This algorithm could be improved by adding cycle 
//detection so a is also adjacent to b in a situation like { c b a c }.
pub fn directed_adjacency_matrix(graph: &Vec::<Vec::<usize>>, total_nodes: usize) -> Vec::<Vec::<usize>> {
    let mut matrix: Vec::<Vec::<usize>> = Vec::with_capacity(total_nodes);
    for _ in 0..total_nodes {
        let mut row = Vec::with_capacity(total_nodes);
        for _ in 0..total_nodes {
            row.push(0);
        }
        matrix.push(row);
    }
    for edge in graph {
        for (i, a) in edge.iter().enumerate() {
            for b in i..edge.len() {
                matrix[*a][edge[b]] = 1;
            }
        }
    }
    matrix
}


pub fn find_all_neighbors(node: usize, adjacency: &Vec::<Vec::<usize>>) -> Vec::<usize> {
    let mut neighbors = Vec::new();

    for (node, adj) in adjacency[node].iter().enumerate() {
        if *adj == 1 {
            neighbors.push(node);
        }
    }
    neighbors
}


