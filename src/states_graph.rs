use crate::hypergraph::Hypergraph;
use crate::rule::Rule;

#[derive(Clone)]
pub struct State {
    pub graph: Hypergraph,
    pub expanded: bool,
    pub generation: usize,
}

impl State {
    pub fn new() -> State {
        State {
            graph: Hypergraph::new(),
            expanded: false,
            generation: 0,
        }
    }

    pub fn from(graph: Hypergraph) -> State {
        State {
            graph,
            expanded: false,
            generation: 0,
        }
    }
}

#[derive(Clone)]
pub struct StatesGraph {
    pub states: Vec::<State>,
    pub adjacency_matrix: Vec::<Vec::<usize>>,
    pub generation: usize,
    pub generation_sizes: Vec::<usize>,
}

impl StatesGraph {
    pub fn new() -> StatesGraph {
        StatesGraph {
            states: Vec::new(),
            adjacency_matrix: Vec::new(),
            generation: 0,
            generation_sizes: Vec::new(),
        }
    }

    pub fn from(graph: Hypergraph) -> StatesGraph {
        StatesGraph {
            states: vec![State::from(graph)],
            adjacency_matrix: vec![vec![0]],
            generation: 1,
            generation_sizes: vec![1],
        }
    }

    pub fn insert(&mut self, graph: Hypergraph, parent: usize) {
        let mut found_at = self.adjacency_matrix.len();
        for (i, state) in self.states.iter().enumerate() {
            if graph.matches(&state.graph) {
                found_at = i;
                break;
            }
        }
        if found_at == self.adjacency_matrix.len() {
            let mut new_row = Vec::new();
            for row in &mut self.adjacency_matrix {
                row.push(0);
                new_row.push(0);
            }
            new_row.push(0);
            self.adjacency_matrix.push(new_row);
            let state = State {
                graph,
                expanded: false,
                generation: self.generation,
            };
            while self.generation >= self.generation_sizes.len() {
                self.generation_sizes.push(0);
            }
            self.generation_sizes[self.generation] += 1;
            self.states.push(state);
        }
        self.adjacency_matrix[parent][found_at] = 1;
    }

    pub fn expand_all(&mut self, rule: &Rule) {
        let mut to_insert = Vec::new();
        for i in 0..self.states.len() {
            if self.states[i].expanded {
                continue;
            }
            let graphs = self.states[i].graph.all_updates(rule);
            self.states[i].expanded = true;
            for graph in graphs {
                to_insert.push((graph,i));
            }
        }
        for (graph, parent) in to_insert {
            self.insert(graph, parent);
        }
        self.generation += 1;
    }
}
