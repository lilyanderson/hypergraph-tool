use std::sync::{mpsc, Arc, Mutex};

use crate::hypergraph::*;
use crate::draw::{GraphElement, StatesAction};
use crate::rule::Rule as GraphRule;
use crate::states_graph::StatesGraph;

use pest::Parser;

#[derive(Parser)]
#[grammar = "parser.pest"]
pub struct MyParser {
    pub graph: Hypergraph,
    pub rule: GraphRule,
    pub states_graph: StatesGraph,
    pub graph_tx: mpsc::Sender::<GraphElement>,
    pub rule_tx: mpsc::Sender::<GraphRule>,
    pub func_tx: mpsc::Sender::<Vec::<f64>>,
    pub state_tx: mpsc::Sender::<StatesAction>,
    pub command_list: Arc::<Mutex::<String>>,
}

impl MyParser {
    //Parse the given text and return a result about the graph for it to be updated
    pub fn my_parse(&mut self, text: &str) -> String {
        let mut res_text = String::from(text);
        let mut result = match MyParser::parse(Rule::action, text) {
            Ok(result) => result,
            Err(e) => { 
                res_text.push_str(&format!("\nInvalid input: {}\n{}", text, e)); 
                return res_text; 
            }
        };
        let mut command_list = self.command_list.lock().unwrap();
        command_list.push_str(text);
        command_list.push('\n');
        let action = result.next().unwrap(); //match rule::action
        let action = action.into_inner().next().unwrap(); //actual command
        match action.as_rule() {
            Rule::action | 
                Rule::rule_side | 
                Rule::edge | 
                Rule::node | 
                Rule::WHITESPACE | 
                Rule::digit | 
                Rule::float
                => {}
            Rule::add_edge => {
                let edge = action.into_inner().next().unwrap();
                let edge = get_edge(edge);
                self.graph.insert_edge(edge);
                self.states_graph = StatesGraph::from(self.graph.clone());
                let _ = self.graph_tx.send(GraphElement::Graph(self.graph.clone()));
                let _ = self.state_tx.send(StatesAction::States(self.states_graph.clone()));
            },
            Rule::delete => {
                let edge = action.into_inner().next().unwrap();
                let edge = get_edge(edge);
                self.graph.delete_edge(edge);
                self.states_graph = StatesGraph::from(self.graph.clone());
                let _ = self.graph_tx.send(GraphElement::Graph(self.graph.clone()));
                let _ = self.state_tx.send(StatesAction::States(self.states_graph.clone()));
            },
            Rule::update => {
                let count = match action.into_inner().next() {
                    Some(node) => get_usize(Some(node)),
                    None => 1,
                };
                for _ in 0..count {
                    self.graph.update(&self.rule);
                }
                let _ = self.graph_tx.send(GraphElement::Graph(self.graph.clone()));
                self.states_graph = StatesGraph::from(self.graph.clone());
            }
            Rule::rule => {
                self.rule = get_rule(action);
                let _ = self.rule_tx.send(self.rule.clone());
            },
            Rule::path => {
                let path = get_path(action, &self.graph);
                if let Some(path) = path {
                    let _ = self.graph_tx.send(GraphElement::Path(path));
                } else {
                    res_text.push_str("\nCould not find path");
                }
            }
            Rule::ball => {
                let paths = get_ball(action, &self.graph);
                for path in paths {
                    let _ = self.graph_tx.send(GraphElement::Path(path));
                }
            }
            Rule::vr => {
                let vr = get_vr(action, &self.graph);
                res_text.push('\n');
                for dist in &vr {
                    res_text.push_str(&format!("{}\t", dist));
                }
                let vr = vr_to_float(&vr);
                let dr = vr_to_dr(&vr);
                res_text.push('\n');
                for delta in &dr {
                    res_text.push_str(&format!("{}\t", *delta));
                }
                let _ = self.func_tx.send(vr);
                let _ = self.func_tx.send(dr);
            }
            Rule::avr => {
                let max = get_usize(action.into_inner().next());
                let adjacency = directed_adjacency_matrix(&self.graph.graph, self.graph.total_nodes);
                let averages = Hypergraph::average_vr(max, &adjacency);
                res_text.push('\n');
                for avg in &averages {
                    res_text.push_str(&format!("{}\t", avg));
                }
                res_text.push('\n');
                let dr = vr_to_dr(&averages);
                for delta in &dr {
                    res_text.push_str(&format!("{}\t", *delta));
                }
                res_text.push('\n');
                let _ = self.func_tx.send(averages);
                let _ = self.func_tx.send(dr);
            }
            Rule::distance => {
                let distance = get_distance(action, &self.graph);
                match distance {
                    Some(dist) => { res_text.push_str(&format!("\n{}", dist)); }
                    None => { res_text.push_str("\nCould not find a path."); }
                }
            }
            Rule::clear => {
                self.graph = Hypergraph::new();
                self.states_graph = StatesGraph::from(self.graph.clone());
                let _ = self.graph_tx.send(GraphElement::Graph(self.graph.clone()));
                let _ = self.state_tx.send(StatesAction::States(self.states_graph.clone()));
            }
            Rule::init => {
                let _ = self.graph_tx.send(GraphElement::Init);
            }
            Rule::step => {
                let _ = self.graph_tx.send(GraphElement::Step);
            }
            Rule::scale => {
                let scale = get_float(action.into_inner().next().unwrap());
                let _ = self.graph_tx.send(GraphElement::Scale(scale));
            }
            Rule::expand => {
                self.states_graph.expand_all(&self.rule);
                let _ = self.state_tx.send(StatesAction::States(self.states_graph.clone()));
            }
            Rule::get_state => {
                let state = get_usize(action.into_inner().next());
                if state >= self.states_graph.states.len() {
                    res_text.push_str("\nThat state does not exist");
                } else {
                    self.graph = self.states_graph.states[state].graph.clone();
                    let _ = self.graph_tx.send(GraphElement::Graph(self.graph.clone()));
                }
            }
        }
        res_text.push('\n');
        res_text
    }
}

fn get_distance(dist: pest::iterators::Pair::<Rule>, graph: &Hypergraph) -> Option::<usize> {
    let (a,b) = get_2_usize(dist);
    let adjacency = directed_adjacency_matrix(&graph.graph, graph.total_nodes);
    let path = Hypergraph::dijkstra(a, b, adjacency);
    match path {
        Some(path) => { let dist = path.len() - 1; Some(dist) }
        None => None
    }
}

fn get_ball(ball: pest::iterators::Pair::<Rule>, graph: &Hypergraph) -> Vec::<Vec::<usize>> {
    let (node, radius) = get_2_usize(ball);
    let adjacency = directed_adjacency_matrix(&graph.graph, graph.total_nodes);
    let paths = Hypergraph::ball(node, radius, &adjacency);
    paths
}

fn get_vr(vr: pest::iterators::Pair::<Rule>, graph: &Hypergraph) -> Vec::<usize> {
    let (node, max) = get_2_usize(vr);
    let adjacency = directed_adjacency_matrix(&graph.graph, graph.total_nodes);
    Hypergraph::find_vr(node, max, &adjacency)
}

fn vr_to_float(vr: &Vec::<usize>) -> Vec::<f64> {
    let mut as_float = Vec::new();
    for num in vr {
        as_float.push(*num as f64);
    }
    as_float
}

fn vr_to_dr(vr: &Vec::<f64>) -> Vec::<f64> {
    let mut dr = Vec::new(); 
    for r in 0..vr.len() - 1 {
        let rfloat = r as f64;
        let delta = (vr[r].ln() - vr[r+1].ln()) / ( (rfloat+2.0).ln() - (rfloat+1.0).ln());
        dr.push(delta);
    }

    dr
}

fn get_path(path: pest::iterators::Pair::<Rule>, graph: &Hypergraph) -> Option::<Vec::<usize>>{
    let (a,b) = get_2_usize(path);
    let adjacency = adjacency_matrix(&graph.graph, graph.total_nodes);
    let result = Hypergraph::dijkstra(a,b,adjacency);
    result
}

fn get_rule(rule: pest::iterators::Pair::<Rule>) -> GraphRule {
    let mut sides = rule.into_inner();
    let lhs = get_side(sides.next());
    let rhs = get_side(sides.next());
    GraphRule::from(lhs,rhs)
}

fn get_side(side: Option::<pest::iterators::Pair::<Rule>>) -> Vec::<Vec::<usize>> {
    let mut result = Vec::new();
    let side = side.unwrap();
    for edge in side.into_inner() {
        result.push(get_edge(edge));
    }
    result
}


fn get_edge(edge: pest::iterators::Pair::<Rule>) -> Vec::<usize> {
    let mut result = Vec::new();
    for node in edge.into_inner() {
        result.push(get_usize(Some(node)));
    }
    result
}

fn get_2_usize(rule: pest::iterators::Pair::<Rule>) -> (usize, usize) {
    let mut usizes = rule.into_inner();
    let a = get_usize(usizes.next());
    let b = get_usize(usizes.next());
    (a,b)
}

fn get_usize(node: Option::<pest::iterators::Pair::<Rule>>) -> usize {
    let node = node.unwrap();
    node.as_str().trim().parse::<usize>().unwrap()
}

fn get_float(float: pest::iterators::Pair::<Rule>) -> f64 {
    float.as_str().trim().parse::<f64>().unwrap()
}
