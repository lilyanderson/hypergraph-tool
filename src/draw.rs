use std::sync::mpsc;

use crate::hypergraph::*;
use crate::rule::Rule;
use crate::states_graph::StatesGraph;

use plotters::prelude::*;

const PI: f64 = 3.1415926535;
const LENGTH: f64 = 10.0;
const ITERATIONS: usize = 1000;
const DELTA_T: f64 = 0.1;
const DAMPING: f64 = 0.95;
const SPRING: f64 = -0.5;
const REPULSION: f64 = 100000.0;

pub struct GraphRender {
    pub graph: Hypergraph,
    pub paths: Vec::<Vec::<usize>>,
    pub points: Vec::<(f64, f64, f64,f64)>,
    pub scale: f64,
    pub rx: mpsc::Receiver::<GraphElement>,
}

impl GraphRender {
    pub fn render(&mut self, width: f64, height: f64, cr: &cairo::Context) {
        while let Ok(elem) = self.rx.try_recv() {
            match elem {
                GraphElement::Graph(graph) => {
                    self.graph = graph;
                }
                GraphElement::Path(path) => {
                    self.paths.push(path);
                }
                GraphElement::Init => {
                    self.points = init_points(self.graph.total_nodes, width, height, self.scale);
                }
                GraphElement::Step => {
                    let adjacency = adjacency_matrix(&self.graph.graph, self.graph.total_nodes);
                    let scale = self.scale;
                    iteration(&mut self.points, &adjacency,width,height, scale);
                }
                GraphElement::Scale(scale) => {
                    self.scale = scale;
                }
            }
        }
        if self.points.len() != self.graph.total_nodes {
            let adjacency = adjacency_matrix(&self.graph.graph, self.graph.total_nodes);
            self.paths = Vec::new();
            self.points = find_points(&adjacency, self.graph.total_nodes, width, height, self.scale);
        }
        cr.scale(self.scale, self.scale);
        draw_graph(cr, &self.graph.graph, &self.points);
        draw_paths(cr, &self.paths, &self.points);
        cr.scale(1.0/self.scale,1.0/self.scale);
    }
}

pub struct RuleRender {
    pub rule: Rule,
    pub lhs_points: Vec::<(f64,f64,f64,f64)>,
    pub rhs_points: Vec::<(f64,f64,f64,f64)>,
    pub rx: mpsc::Receiver::<Rule>,
}

impl RuleRender {
    pub fn render(&mut self, width: f64, height: f64, cr: &cairo::Context) {
        let mut updated = false;
        while let Ok(rule) = self.rx.try_recv() {
            self.rule = rule;
            updated = true;
        }
        if updated {
            let lhs_adj = adjacency_matrix(&self.rule.lhs, self.rule.lhs_nodes);
            let rhs_adj = adjacency_matrix(&self.rule.rhs, self.rule.rhs_nodes);
            self.lhs_points = find_points(&lhs_adj, self.rule.lhs_nodes, width/2.0, height, 1.0);
            self.rhs_points = find_points(&rhs_adj, self.rule.rhs_nodes, width/2.0, height, 1.0);
        }
        draw_graph(cr, &self.rule.lhs, &self.lhs_points);
        cr.translate(width/2.0, 0.0);
        draw_graph(cr, &self.rule.rhs, &self.rhs_points);
    }
}

pub struct FunctionRender {
    pub functions: Vec::<Vec::<f64>>,
    pub max_x: f64,
    pub max_y: f64,
    pub min_y: f64,
    pub rx: mpsc::Receiver::<Vec::<f64>>,
}

impl FunctionRender {
    pub fn render(&mut self, _width: f64, _height: f64, cr: &cairo::Context) {
        //Recieve new functions
        while let Ok(function) = self.rx.try_recv() {
            if function.len() as f64 > self.max_x {
                self.max_x = function.len() as f64;
            }
            for val in &function {
                if *val > self.max_y {
                    self.max_y = *val;
                }
                if *val < self.min_y {
                    self.min_y = *val;
                }
            }
            self.functions.push(function);
        }

        //Set up chart
        let root = CairoBackend::new(cr, (500,500)).unwrap().into_drawing_area();
        root.fill(&WHITE).unwrap();
        let mut chart = ChartBuilder::on(&root)
            .x_label_area_size(40)
            .y_label_area_size(40)
            .build_ranged(0.0..self.max_x,self.min_y..self.max_y)
            .unwrap();
        chart.configure_mesh()
            .draw()
            .unwrap();
        //Draw functions
        for function in &self.functions {
            chart.draw_series(LineSeries::new((0..function.len()).map(|x| (x as f64, function[x])), &RED)).unwrap();
        }
    }
}

pub struct StatesRender {
    pub states: StatesGraph,
    pub points: Vec::<(f64, f64)>,
    pub scale: f64,
    pub rx: mpsc::Receiver::<StatesAction>,
}

pub enum StatesAction {
    States(StatesGraph),
}

impl StatesRender {
    pub fn render(&mut self, width: f64, height: f64, cr: &cairo::Context) {
        while let Ok(action) = self.rx.try_recv() {
            match action {
                StatesAction::States(states) => { self.states = states; }
            }
        }
        if self.points.len() != self.states.adjacency_matrix.len() {
            self.points = states_points(&self.states, width, height);
        }
        cr.scale(self.scale, self.scale);
        draw_states(cr, &self.states.adjacency_matrix, &self.points);
        cr.scale(1.0/self.scale,1.0/self.scale);
    }
}

//Represents different things that may be passed to update the state of the draw
//methods
pub enum GraphElement {
    Graph(Hypergraph),
    Path(Vec::<usize>),
    Init,
    Step,
    Scale(f64),
}

pub fn states_points(states: &StatesGraph, width: f64, height: f64) -> Vec::<(f64, f64)> {
    let rows = states.generation as f64;
    let mut row = 0;
    let mut count = 0.0;
    let mut points = Vec::new();

    for state in &states.states {
        if row != state.generation {
            row = state.generation;
            count = 0.0;
        }
        points.push((width * count / states.generation_sizes[row] as f64, height * row as f64 / rows));
        count += 1.0;
    }
    points
}

pub fn draw_states(cr: &cairo::Context, adjacency: &Vec::<Vec::<usize>>, points: &Vec::<(f64,f64)> ) {
    for (node, (x,y)) in points.iter().enumerate() {
        cr.move_to(*x, *y);
        cr.show_text(&format!("{}", node));
    }
    for (node_a, row) in adjacency.iter().enumerate() {
        let (x,y) = points[node_a];
        for (node_b, adj) in row.iter().enumerate() {
            if *adj == 0 || node_a == node_b {
                continue;
            }
            cr.move_to(x,y);
            let (x2,y2) = points[node_b];
            cr.line_to(x2,y2);
            cr.stroke();
            //Draw arrows
            if (x,y) == (x2,y2) {
                continue;
            }
            let dx = x-x2;
            let dy = y-y2;
            let angle = dy.atan2(dx);
            draw_arrow(cr, x, y, angle);
        }
    }
}

//Draws the hypergraph to the screen
pub fn draw_graph(cr: &cairo::Context, graph: &Vec::<Vec::<usize>>, points: &Vec::<(f64, f64,f64,f64)>) {
    //Draw name of node to screen, to make identification easier.
    for (node, (x,y,_,_)) in points.iter().enumerate() {
        cr.move_to(*x,*y);
        cr.show_text(&format!("{}", node));
    }
    //Draw the actual edges.
    for edge in graph {
        //Sanity check
        if edge.len() == 0 {
            continue;
        } else if edge.len() == 1 {
            //Unary self loop
            let (x,y,_,_) = points[edge[0]];
            cr.arc(x,y,20.0, 0.0, 2.0*PI);
            cr.stroke();
            continue;
        } else {
            draw_edge(cr, edge, points);
        }
    }
}

//Draw a hyperedge, lines showing directedness and fill showing edge degree
pub fn draw_edge(cr: &cairo::Context, edge: &Vec::<usize>, points: &Vec::<(f64, f64,f64,f64)>) {
    //Draw the edge itself
    let (x,y,_,_) = points[edge[0]];
    cr.move_to(x,y);
    for node in edge {
        let (x,y,_,_) = points[*node];
        cr.line_to(x,y);
    }
    cr.stroke_preserve();
    cr.set_source_rgba(0.0,0.0,255.0,0.8);
    cr.fill();
    cr.set_source_rgba(0.0,0.0,0.0,1.0);

    //Draw loops
    let mut prev = None;
    for node in edge {
        if prev == Some(node) {
            let (x,y,_,_) = points[*node];
            cr.arc(x+20.0, y, 20.0, 0.0, 2.0*PI);
            cr.stroke_preserve();
            cr.set_source_rgba(0.0,0.0,255.0,0.8);
            cr.fill();
            cr.set_source_rgba(0.0,0.0,0.0,1.0);
            cr.move_to(x,y);
            cr.line_to(x,y+18.0);
            cr.line_to(x+12.0, y+14.0);
            cr.line_to(x,y);
            cr.fill();
        }
        prev = Some(node);
    }

    //Draw arrows
    let (mut px, mut py,_,_) = points[edge[0]];
    for node in edge {
        let (x,y,_,_) = points[*node];
        if (px,py) == (x,y) {
            continue;
        }
        let dx = x-px;
        let dy = y-py;
        let angle = dy.atan2(dx);
        draw_arrow(cr, x, y, angle);
        px = x;
        py = y;
    }
}

//Draw an arrow pointing along angle.
pub fn draw_arrow(cr: &cairo::Context, x: f64, y: f64, angle: f64) {
    let xa = angle.cos() * 25.0;
    let ya = angle.sin() * 25.0;
    let h1 = angle.sin() * -10.0;
    let h2 = angle.cos() * 10.0;
    cr.move_to(x,y);
    cr.line_to(x-xa, y-ya);
    cr.line_to(x-xa+h1,y-ya+h2);
    cr.line_to(x,y);
    cr.fill();

}

//For every path in paths, draw a both red line over the path.
pub fn draw_paths(cr: &cairo::Context, paths: &Vec::<Vec::<usize>>, points: &Vec::<(f64, f64,f64,f64)>) {
    cr.set_source_rgba(255.0, 0.0, 0.0, 1.0);
    cr.set_line_width(3.0);
    for path in paths {
        let (x,y,_,_) = points[path[0]];
        cr.move_to(x,y);
        for node in path {
            let (x,y,_,_) = points[*node];
            cr.line_to(x,y);
        }
        cr.stroke();
    }
    cr.set_source_rgba(0.0,0.0,0.0,1.0);
    cr.set_line_width(1.0);
}

//Calculates a set of points by running them through many rounds of physics
//calculations to adjust their position
pub fn find_points(adjacency: &Vec::<Vec::<usize>>, nodes: usize, width: f64, height: f64, scale: f64) -> Vec::<(f64,f64,f64,f64)> {
    let mut rs = init_points(nodes, width, height, scale);
    for _ in 0..ITERATIONS {
        iteration(&mut rs, &adjacency, width, height, scale);
    }
    rs
}

//Performs one round of calculating the force on every node and applying
//verlet integration to find their next position in space.
pub fn iteration(rs: &mut Vec::<(f64,f64,f64,f64)>, adjacency: &Vec::<Vec::<usize>>, width: f64, height: f64, scale: f64) {
    let accels = accels(&rs, &adjacency);
    for node in 0..rs.len() {
        let (mut x,mut y,px,py) = rs[node];
        let (ax,ay) = accels[node];
        //Verlet integration
        let mut nx = x + (x - px)*DAMPING + 0.5*ax*DELTA_T*DELTA_T;
        let mut ny = y + (y - py)*DAMPING + 0.5*ay*DELTA_T*DELTA_T;
        if nx > width / scale {
            nx = width / scale;
            x = width / scale;
        } else if nx < 0.0 {
            nx = 0.0;
            x = 0.0;
        }
        if ny > height / scale {
            ny = height / scale;
            y = height / scale;
        } else if ny < 0.0 {
            ny = 0.0;
            y = 0.0;
        }
        rs[node] = (nx,ny,x,y);
    }
}

//Generates a set of points for every node and sets their previous value to their
//starting location to begin with.
pub fn init_points(total_nodes: usize, width: f64, height: f64, scale: f64) -> Vec::<(f64,f64,f64,f64)> {
    let mut points = Vec::new();
    let min = if height < width {
        height
    } else {
        width
    };
    let radius = min * 0.75 * 0.5;
    for node in 0..total_nodes {
        let frac = node as f64 / total_nodes as f64;
        let angle = 2.0 * PI * frac;
        let xi = width/2.0 + angle.cos()*radius;
        let yi = height/2.0 + angle.sin()*radius;
        points.push((xi/scale,yi/scale,xi/scale,yi/scale));
    }
    points
}

//Calculates the acceleration every node will feel when subjected to a repulsive
//(inverse square) force by all nodes and a spring force by neighbors.
pub fn accels(points: &Vec::<(f64,f64,f64,f64)>, adjacency: &Vec::<Vec::<usize>>) -> Vec::<(f64,f64)> {
    let mut accels = Vec::new();
    for a in 0..points.len() {
        accels.push((0.0,0.0));

        for b in 0..points.len() {
            let (x1, y1,_,_) = points[a];
            let (x2, y2,_,_) = points[b];
            if x1 == x2 && y1 == y2 {
                continue;
            }
            let dx = x1-x2;
            let dy = y1-y2;
            let rsqrd = dx*dx + dy*dy;
            let r = rsqrd.sqrt();
            let rx = dx / r;
            let ry = dy / r;
            accels[a].0 += REPULSION * rx / rsqrd;
            accels[a].1 += REPULSION * ry / rsqrd;

            if adjacency[a][b] == 1 {
                let angle = dy.atan2(dx);
                let x_rest = LENGTH * angle.cos();
                let y_rest = LENGTH * angle.sin();
                let x_s = dx - x_rest;
                let y_s = dy - y_rest;
                accels[a].0 += SPRING * x_s;
                accels[a].1 += SPRING * y_s;
            }
        }
    }
    accels
}

