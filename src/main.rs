extern crate gtk;

use std::process;
use std::sync::{mpsc, Arc, Mutex};
use std::cell::RefCell;
use std::path::PathBuf;
use std::fs::File;
use std::io::{BufReader, BufRead, Write};

use hypergraph_tool::hypergraph::Hypergraph;
use hypergraph_tool::draw::*;
use hypergraph_tool::parse::MyParser;
use hypergraph_tool::rule::Rule;
use hypergraph_tool::states_graph::StatesGraph;

use gtk::*;
use gtk::prelude::*;

const APP_NAME: &str = "Hypergraph Tool";

struct App {
    window: Window,
    header: Header,
    content: Content,
}

impl App {
    fn new() -> App {
        //Create ui elements
        let window = Window::new(WindowType::Toplevel);
        let header = Header::new();
        let content = Content::new();

        //Setup ui elements
        window.set_titlebar(Some(&header.container));
        window.set_title(APP_NAME);
        window.set_wmclass(APP_NAME, APP_NAME);
        window.set_size_request(400,200);
        window.add(&content.container);

        //Connect ui to data and events.
        window.connect_delete_event(move |_,_| {
            main_quit();
            Inhibit(false)
        });
        //Ability to save work. Get file, create it and write commands.
        let to_save = content.to_save.clone();
        header.save.connect_clicked(move |_save| {
            let file = get_file(FileChooserAction::Save);
            if let Some(file) = file {
                let mut file = File::create(file).unwrap();
                let command_list = to_save.lock().unwrap();
                file.write(command_list.as_bytes()).unwrap();
            }
        });
        //Ability to open old work. Get file, open it, read it and process commands.
        let parser = content.parser.clone();
        let out = content.output.clone();
        header.open.connect_clicked(move |_open| {
            let file = get_file(FileChooserAction::Open);
            if let Some(file) = file {
                let file = File::open(file).unwrap();
                let mut reader = BufReader::new(file);
                let mut contents = String::new();
                let mut parser = parser.lock().unwrap();

                while let Ok(code) = reader.read_line(&mut contents) {
                    if code == 0 {
                        break;
                    }
                    parser.my_parse(contents.as_str());
                    let mut end = out.get_end_iter();
                    out.insert(&mut end, &contents.as_str());
                    contents.clear();
                }
            }
        });

        App { window, header, content }
    }
}

struct Header {
    container: HeaderBar,
    save: Button,
    open: Button,
}

impl Header {
    fn new() -> Header {
        //Create ui elements
        let container = HeaderBar::new();
        let save = Button::new_from_icon_name(Some("document-save-symbolic"),
            IconSize::SmallToolbar);
        let open = Button::new_from_icon_name(Some("document-open-symbolic"),
            IconSize::SmallToolbar);

        //Setup ui elements
        container.set_title(Some(APP_NAME));
        container.set_show_close_button(true);
        container.add(&save);
        container.add(&open);
        Header { container, save, open }
    }
}

struct Content {
    container: Paned,
    output: TextBuffer,
    to_save: Arc::<Mutex::<String>>,
    parser: Arc::<Mutex::<MyParser>>,
}

impl Content {
    fn new() -> Content {
        //Create GUI elements
        let container = Paned::new(Orientation::Horizontal);
        //Input and Text elements on left pane
        let left_pane = Box::new(Orientation::Vertical, 4);
        let input = Entry::new();
        let in_label = Label::new(Some("Input"));
        let output = TextBuffer::new(Some(&TextTagTable::new()));
        let output_view = TextView::new_with_buffer(&output);
        let out_label = Label::new(Some("Output"));
        let scroller = ScrolledWindow::new::<Adjustment, Adjustment>(None, None);
        let out = output.clone();
        //Notebook elements on right pane.
        let notebook = Notebook::new();
        let graph_draw = DrawingArea::new();
        let graph_l = Label::new(Some("Hypergraph"));
        let rule_draw = DrawingArea::new();
        let rule_l = Label::new(Some("Rule"));
        let function_draw = DrawingArea::new();
        let function_l = Label::new(Some("Function Graph"));
        let states_draw = DrawingArea::new();
        let states_l = Label::new(Some("States Graph"));

        //Set up gui elements on left pane
        in_label.set_halign(Align::Center);
        out_label.set_halign(Align::Center);
        output_view.set_editable(false);
        left_pane.set_size_request(200, 200);

        //Place gui elements
        //Elements on left
        scroller.add(&output_view);
        left_pane.pack_start(&in_label, false, true, 0);
        left_pane.pack_start(&input, false, true, 0);
        left_pane.pack_start(&out_label, false, true, 0);
        left_pane.pack_start(&scroller, true, true, 0);
        //Notebook elements
        notebook.append_page(&graph_draw, Some(&graph_l));
        notebook.append_page(&rule_draw, Some(&rule_l));
        notebook.append_page(&function_draw, Some(&function_l));
        notebook.append_page(&states_draw, Some(&states_l));
        container.pack1(&left_pane, true, true);
        container.pack2(&notebook, true, true);

        //Create data elements
        let (graph_tx, graph_rx) = mpsc::channel::<GraphElement>();
        let (rule_tx, rule_rx) = mpsc::channel::<Rule>();
        let (func_tx, func_rx) = mpsc::channel::<Vec::<f64>>();
        let (state_tx, state_rx) = mpsc::channel::<StatesAction>();
        let graph = Hypergraph::new();
        let graph_render = GraphRender { 
            graph: graph.clone(), 
            paths: Vec::new(), 
            points: Vec::new(),
            scale: 1.0,
            rx: graph_rx,
        };
        let graph_render = RefCell::new(graph_render);
        let rule_render = RuleRender {
            rule: Rule::new(),
            lhs_points: Vec::new(),
            rhs_points: Vec::new(),
            rx: rule_rx,
        };
        let rule_render = RefCell::new(rule_render);
        let function_render = FunctionRender {
            functions: Vec::new(),
            max_x: 1.0,
            max_y: 1.0,
            min_y: 0.0,
            rx: func_rx,
        };
        let function_render = RefCell::new(function_render);
        let states_render = StatesRender {
            states: StatesGraph::new(),
            points: Vec::new(),
            scale: 1.0,
            rx: state_rx,
        };
        let states_render = RefCell::new(states_render);
        let rule = Rule::new();
        let states_graph = StatesGraph::new();
        let command_list = Arc::new(Mutex::new(String::new()));
        let to_save = command_list.clone();
        let parser = MyParser { 
            graph, 
            rule,
            states_graph,
            graph_tx, 
            rule_tx, 
            func_tx, 
            state_tx, 
            command_list 
        };
        let parser = Arc::new(Mutex::new(parser));
        let parse = parser.clone();

        //Connect data to events
        let graph_drawer = graph_draw.clone();
        let rule_drawer = rule_draw.clone();
        let function_drawer = function_draw.clone();
        let states_drawer = states_draw.clone();
        input.connect_key_press_event(move |widget, key| {
            let mut parser = parse.lock().unwrap();
            if key.get_keyval() == 65293 { //Key code for enter
                let input = widget.get_text();
                let input = input.unwrap();
                let input = input.as_str();
                let res_text = parser.my_parse(input);
                let mut end = out.get_end_iter();
                out.insert(&mut end, &res_text);

            }
            graph_drawer.queue_draw();
            rule_drawer.queue_draw();
            function_drawer.queue_draw();
            states_drawer.queue_draw();
            Inhibit(false)
        });

        graph_draw.connect_draw(move |widget, cr| {
            let mut graph_render = graph_render.borrow_mut();
            let width = widget.get_allocated_width() as f64;
            let height = widget.get_allocated_height() as f64;
            graph_render.render(width, height, cr);
            Inhibit(false)
        });
        rule_draw.connect_draw(move |widget, cr| {
            let mut rule_render = rule_render.borrow_mut();
            let width = widget.get_allocated_width() as f64;
            let height = widget.get_allocated_height() as f64;
            rule_render.render(width, height, cr);
            Inhibit(false)
        });
        function_draw.connect_draw(move |widget, cr| {
            let mut function_render = function_render.borrow_mut();
            let width = widget.get_allocated_width() as f64;
            let height = widget.get_allocated_height() as f64;
            function_render.render(width, height, cr);
            Inhibit(false)
        }); 
        states_draw.connect_draw(move |widget, cr| {
            let mut states_render = states_render.borrow_mut();
            let width = widget.get_allocated_width() as f64;
            let height = widget.get_allocated_height() as f64;
            states_render.render(width, height, cr);
            Inhibit(false)
        });

        Content { container, output, to_save, parser }
    }
}

//Open a file chooser and return the selected file to the caller.
fn get_file(action: FileChooserAction) -> Option::<PathBuf> {
    //Type of file chooser.
    let name = match action {
        FileChooserAction::Open => "Open",
        FileChooserAction::Save => "Save",
        _ => "Error",
    };

    //Create it
    let dialog = FileChooserDialog::new(
        Some(name),
        Some(&Window::new(WindowType::Popup)),
        action
    );
    
    //Setup elements
    dialog.add_button("Cancel", ResponseType::Cancel.into());
    dialog.add_button(name, ResponseType::Ok.into());
    
    //Run it so the user can select a file.
    dialog.run();
    let file = dialog.get_filename();
    //Remove the chooser from the screen.
    dialog.destroy();
    file
}

// Start the gtk application
fn main() {
    if gtk::init().is_err() {
        eprintln!("Failed to initialize GTK Application");
        process::exit(1);
    }

    let app = App::new();
    app.window.show_all();
    gtk::main();
}
