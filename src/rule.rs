use std::collections::HashMap;

//Represents an updating rule for the Wolfram model.
#[derive(Clone)]
pub struct Rule {
    pub lhs: Vec::<Vec::<usize>>,
    pub lhs_nodes: usize,
    pub rhs: Vec::<Vec::<usize>>,
    pub rhs_nodes: usize,
}

impl Rule {
    pub fn new() -> Rule {
        Rule { 
            lhs: Vec::new(),
            lhs_nodes: 0,
            rhs: Vec::new(),
            rhs_nodes: 0,
        }
    }

    pub fn from(lhs: Vec::<Vec::<usize>>, rhs: Vec::<Vec::<usize>>) -> Rule {
        let lhs_nodes = count_nodes(&lhs);
        let rhs_nodes = count_nodes(&rhs);
        Rule { lhs, lhs_nodes, rhs, rhs_nodes }
    }

    //Takes self and returns a new rule which is isomorphic to self and in 
    //cannonical form. 
    //
    //Cannonical form assumes rules are sorted in decreasing
    //arity and then by edges which introduce the least number of (distinct)
    //new nodes. Nodes are further named in the order they occur within the
    //rule, so the first node 2 will always be before the first node 3.
    pub fn find_cannonical_form(&self) -> Rule {
        let mut left_parts = get_rule_parts(&self.lhs);
        let mut right_parts = get_rule_parts(&self.rhs);
        let mut mapping = HashMap::new();
        let mut next = 0;

        let lhs = get_sorted(&mut left_parts, &mut mapping, &mut next);
        let rhs = get_sorted(&mut right_parts, &mut mapping, &mut next);

        Rule::from(lhs, rhs)
    }

    pub fn create_rhs(&self, map: &HashMap::<usize, usize>, next_node: usize) -> (Vec::<Vec::<usize>>, usize) {
        let mut result = Vec::new();
        let mut next = next_node;
        let mut map = map.clone();
        for edge in &self.rhs {
            let mut edge_result: Vec::<usize> = Vec::new();
            for node in edge {
                if let Some(n) = map.get(&node) {
                    edge_result.push(*n);
                } else {
                    edge_result.push(next);
                    map.insert(*node, next);
                    next += 1;
                }
            }
            result.push(edge_result);
        }
        (result, next)
    }
}

//Takes one side of a given rule and generates the parts that make it up, so
//that each edge is grouped only with edges of the same size.
pub fn get_rule_parts(side: &Vec::<Vec::<usize>>) -> Vec::<Vec::<Vec::<usize>>> {
    let mut side = side.clone();
    if side.len() == 0 {
        return Vec::new();
    }
    side.sort_by(|a,b| {
        b.len().cmp(&a.len())
    });

    let mut parts = Vec::new();
    let mut part = Vec::new();
    let mut len = side[0].len();
    for edge in side {
        if edge.len() != len {
            parts.push(part);
            part = Vec::new();
            len = edge.len();
        }
        part.push(edge);
    }
    parts.push(part);
    parts
}

//Takes a collection of edges and sorts them so that the least number of 
//(distinct) new nodes is added at any one time, with the caveat that the 
//edges are sorted from largest to smallest arity first.
pub fn get_sorted(parts: &mut Vec::<Vec::<Vec::<usize>>>, mapping: &mut HashMap::<usize, usize>, next: &mut usize) -> Vec::<Vec::<usize>> {
    let mut sorted = Vec::new();
    for mut part in parts {
        while part.len() != 0 {
            let least = get_least_new(&mut part, &mapping);
            let mut res = Vec::new();
            for node in least {
                match mapping.get(&node) {
                    Some(result) => { res.push(*result); }
                    None => {
                        mapping.insert(node, *next);
                        res.push(*next);
                        *next += 1;
                    }
                }
            }
            sorted.push(res);
        }
    }
    sorted
}

//Finds which edge requires the least new distinct nodes be mapped in order to 
//insert it.
fn get_least_new(part: &mut Vec::<Vec::<usize>>, mapped: &HashMap::<usize, usize>) -> Vec::<usize> {
    let mut least = 0;
    let (mut least_distinct, mut least_total) = new_needed(&part[0], mapped);
    for (index, edge) in part.iter().enumerate() {
        let (needed_distinct, needed_total) = new_needed(edge, mapped);
        if needed_distinct < least_distinct || (needed_distinct == least_distinct && needed_total > least_total) {
            least = index;
            least_distinct = needed_distinct;
            least_total = needed_total;
        }
    }
    part.remove(least)
}

//Calculates how many distinct and total nodes would need to be mapped in order
//to insert edge.
fn new_needed(edge: &Vec::<usize>, mapped: &HashMap::<usize, usize>) -> (usize, usize) {
    let mut distinct = 0;
    let mut total = 0;
    let mut copy = mapped.clone();
    for node in edge {
        if !copy.contains_key(node) {
            copy.insert(*node, 0);
            distinct += 1;
        }
    }

    for node in edge {
        if !mapped.contains_key(node) {
            total += 1;
        }
    }

    (distinct, total)
}

pub fn count_nodes(side: &Vec::<Vec::<usize>>) -> usize {
    let mut max = 0;
    for edge in side {
        for node in edge {
            if *node >= max {
                max = *node + 1;
            }
        }
    }
    max
}


